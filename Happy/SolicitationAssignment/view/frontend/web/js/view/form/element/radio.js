define([
    'Magento_Ui/js/form/element/abstract',
    'jquery'
], function(Abstract, $) {
    'use strict';

    return Abstract.extend({
        initialize: function () {
            this._super();

            return this;
        },

        /**
         * Handle radio click, return true to check te radio
         */
        click: function(data, event) {
            this.change(event.target.value);

            return true;
        },

        /**
         * Change value of radio
         */
        change: function(value) {

            var company = $('[name="shippingAddress.company"],[name="billingAddresscheckmo.company"]'),
                companyReference = $('[name="shippingAddress.custom_attributes.company_reference"],[name="billingAddress.custom_attributes.company_reference"]'),
                vatId = $('[name="shippingAddress.vat_id"],[name="billingAddresscheckmo.vat_id"]');

            if (value === 'business') {
                company.show();
                companyReference.show();
                vatId.show();

            } else if (value === 'private') {
                company.hide();
                companyReference.hide();
                vatId.hide();
            }
        }
    });
});