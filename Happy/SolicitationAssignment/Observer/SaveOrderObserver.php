<?php
namespace Happy\SolicitationAssignment\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SaveOrderObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        /**
         * Set data for custom attribute
         */
        $order->setData('company_reference', $quote->getCompanyReference());

        return $this;
    }
}
