<?php
namespace Happy\SolicitationAssignment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(
		SchemaSetupInterface $setup, 
		ModuleContextInterface $context
	) {
        $setup->startSetup();
        $quote = $setup->getTable('quote');
        $salesOrder = $setup->getTable('sales_order');
		
		// Set add column for Company Reference in sales_order and quote
		$setup->getConnection()->addColumn(
			$quote,
			'company_reference',
			[
				'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				'nullable' => true,
				'comment' =>'Company Reference'
			]
		);
		
        $setup->endSetup();
    }
}
