<?php
namespace Happy\SolicitationAssignment\Plugin\Checkout\Model;

class ShippingInformationManagement
{
    protected $quoteRepository;

    protected $dataHelper;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }
    
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
		if(!$extensionAttributes = $addressInformation->getExtensionAttributes()) {
            return;
        }
        /**
         * Get current cart and set the custom attribute
         */
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setCompanyReference($extensionAttributes->getCompanyReference());
    }
}
