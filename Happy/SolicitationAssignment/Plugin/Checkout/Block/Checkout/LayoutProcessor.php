<?php
namespace Happy\SolicitationAssignment\Plugin\Checkout\Block\Checkout;
class LayoutProcessor
{
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $customAttributeCode = 'company_reference';

        /**
         * Shipping Custom field for Company Rererence
         */
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['company_reference'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'id' => 'company_reference'
            ],
            'dataScope' => 'shippingAddress.custom_attributes.' . $customAttributeCode,
            'label' => 'Company Reference',
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => [
                'required-entry' => false
            ],
            'sortOrder' => 4,
        ];

        /**
         * Shipping Form Sort Order
         */

        // Change Sort order for Company field
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['company']['sortOrder'] = 1;
        
        // Change Sort order for VAT field
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['vat_id']['sortOrder'] = 2;
 
        /**
         * Payment List
         */
        $configuration = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'];

        /**
         * Billing Custom field for Company Rererence
         */
        foreach ($configuration as $paymentGroup => $groupConfig) {
            if (isset($groupConfig['component']) && $groupConfig['component'] === 'Magento_Checkout/js/view/billing-address') {
    
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company_reference'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'billingAddress.custom_attributes',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/input',
                        'id' => 'company_reference'
                    ],
                    'dataScope' => 'billingAddress.custom_attributes.' . $customAttributeCode,
                    'label' => 'Company Reference',
                    'provider' => 'checkoutProvider',
                    'visible' => true,
                    'validation' => [
                        'required-entry' => false
                    ],
                    'sortOrder' => 4
                ];
            }
        }

        /**
         * Billing Form Sort Order
         */
        foreach ($configuration as $paymentGroup => $groupConfig) {
            // Change Sort order for Company field
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company']['sortOrder'] = 1;
            // Change Sort order for VAT field
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['vat_id']['sortOrder'] = 2;
        }
 
        return $jsLayout;
    }
}
