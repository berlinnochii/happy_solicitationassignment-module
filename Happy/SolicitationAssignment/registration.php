<?php
/**
 * Copyright © Happy Horizon All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Happy_SolicitationAssignment', __DIR__);

